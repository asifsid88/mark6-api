#!/bin/bash
echo 'Stopping tomcat'
touch temp.txt
lsof -n -i4TCP:9090 > temp.txt
tomcatProcess=`sed -n '2p' temp.txt`
tomcatProcessId=$(echo $tomcatProcess | awk '{print $2}')
if [[ -n $tomcatProcessId ]]
then
    echo 'Tomcat running with PID='$tomcatProcessId
    kill -9 $tomcatProcessId
    echo 'Tomcat running at 9090 is stopped.'
else
    echo 'Your tomcat is already stopped.'
fi


lsof -n -i4TCP:8000 > temp.txt
debugProcess=`sed -n '2p' temp.txt`
debugProcessId=$(echo $debugProcess | awk '{print $2}')
if [[ -n $debugProcessId ]]
then
    echo 'Debug Mode running with PID='$debugProcessId
    kill -9 $debugProcessId
    echo 'Debug Mode running at 8000 is stopped.'
else
    echo 'Your Debug Mode is already stopped.'
fi

mvnDebug clean install package -Dmaven.tomcat.port=9090 -Denv=dev tomcat7:run