#!/bin/bash
echo 'Deleting target folder'
rm -rf target

echo 'Building and packaging Mark6 API'
mvn clean compile install package


echo 'Stopping tomcat'
touch temp.txt
lsof -n -i4TCP:9090 > temp.txt
tomcatProcess=`sed -n '2p' temp.txt`
tomcatProcessId=$(echo $tomcatProcess | awk '{print $2}')
if [[ -n $tomcatProcessId ]]
then
    echo 'Tomcat running with PID='$tomcatProcessId
    kill -9 $tomcatProcessId
    echo 'Tomcat running at 9090 is stopped.'
else
    echo 'Your tomcat is already stopped.'
fi


echo 'Run Mark6 API'
java -jar -Denv=dev target/dependency/webapp-runner.jar --port 9090 target/*.war

echo 'Mark6 API running on: http://localhost:9090/'