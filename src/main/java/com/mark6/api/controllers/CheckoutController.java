package com.mark6.api.controllers;

import com.google.gson.Gson;
import com.mark6.api.constants.URLMapping;
import com.mark6.api.helper.ResponseBuilder;
import com.mark6.api.helper.Util;
import com.mark6.api.model.*;
import com.mark6.api.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by mhussaa on 9/11/17.
 */
@Controller
@RequestMapping(URLMapping.CHECKOUT_ROOT)
public class CheckoutController {

    private CheckoutService checkoutService;
    private ProductService productService;
    private CartService cartService;
    private CustomerService customerService;
    private OrderService orderService;

    @Autowired
    public CheckoutController(CheckoutService checkoutService, ProductService productService,
                              CartService cartService, CustomerService customerService,
                              OrderService orderService) {
        this.checkoutService = checkoutService;
        this.productService = productService;
        this.cartService = cartService;
        this.customerService = customerService;
        this.orderService = orderService;
    }

    @RequestMapping(value = URLMapping.CHECKOUT_INIT, method = RequestMethod.GET)
    public @ResponseBody Response checkoutInit(@RequestParam(URLMapping.PARAM_CUSTOMER_ID) String customerId,
                                               @RequestParam(URLMapping.PARAM_PRODUCT_ID) String productId) {
        Cart cart;
        if(!Util.isNullOrEmpty(productId)) {
            ProductDetail productDetail = productService.findProductByStyleCode(productId.split("::")[0]);
            cart = cartService.createTransientCartRef(customerId, productId, productDetail);
        } else {
            cart = cartService.getCartRef(customerId);
        }

        Customer customer = customerService.getCustomerById(customerId);
        Checkout checkout = checkoutService.init(cart, customer);

        return ResponseBuilder.buildResponse(checkout);
    }


    @RequestMapping(value = URLMapping.CHECKOUT_SELECT_ADDRESS, method = RequestMethod.GET)
    public @ResponseBody Response selectAddress(@RequestParam(URLMapping.PARAM_CUSTOMER_ID) String customerId,
                                                @RequestParam(URLMapping.PARAM_ADDRESS_ID) String addressId) {
        Address address = customerService.getCustomerAddressById(customerId, addressId);
        return ResponseBuilder.buildResponse(address);
    }

    @RequestMapping(value = URLMapping.PLACE_ORDER, method = RequestMethod.POST)
    public @ResponseBody Response placeOrder(@RequestBody Map<String, Object> checkoutObjData) {
        Object checkoutObj = checkoutObjData.get(URLMapping.PARAM_CHECKOUT_OBJ);
        Checkout checkout = new Checkout().fromJSON(new Gson().toJson(checkoutObj));

        String orderId = orderService.placeOrder(checkout);
        cartService.deleteCart(checkout.getCartRefId());
        checkoutService.deleteCheckout(checkout.getId());

        return ResponseBuilder.buildResponse(orderId);
    }
}
