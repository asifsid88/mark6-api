package com.mark6.api.controllers;

import com.mark6.api.constants.URLMapping;
import com.mark6.api.helper.ResponseBuilder;
import com.mark6.api.model.ProductDetail;
import com.mark6.api.model.Response;
import com.mark6.api.model.Wishlist;
import com.mark6.api.services.ProductService;
import com.mark6.api.services.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by mhussaa on 9/11/17.
 */
@Controller
@RequestMapping(URLMapping.WISHLIST_ROOT)
public class WishlistController {

    private WishlistService wishlistService;
    private ProductService productService;

    @Autowired
    public WishlistController(WishlistService wishlistService, ProductService productService) {
        this.wishlistService = wishlistService;
        this.productService = productService;
    }

    @RequestMapping(value = URLMapping.ADD_TO_WISHLIST, method = RequestMethod.GET)
    public @ResponseBody Response addToWishlist(@RequestParam(URLMapping.PARAM_CUSTOMER_ID) String customerId,
                                                @RequestParam(URLMapping.PARAM_PRODUCT_ID) String productId) {
        ProductDetail productDetail = productService.getProductById(productId);
        Wishlist wishlist = wishlistService.addToWishlist(customerId, productId, productDetail);
        return ResponseBuilder.buildResponse(wishlist);
    }

    @RequestMapping(value = URLMapping.GET_WISHLIST, method = RequestMethod.GET)
    public @ResponseBody Response getWishlist(@RequestParam(URLMapping.PARAM_CUSTOMER_ID) String customerId) {
        Wishlist wishlist = wishlistService.getWishlist(customerId);
        return ResponseBuilder.buildResponse(wishlist);
    }

    @RequestMapping(value = URLMapping.REMOVE_WISHLIST_ITEM, method = RequestMethod.GET)
    public @ResponseBody Response removeWishlistItem(@RequestParam(URLMapping.PARAM_CUSTOMER_ID) String customerId,
                                                     @RequestParam(URLMapping.PARAM_PRODUCT_ID) String productId) {
        Wishlist wishlist = wishlistService.removeWishlistItem(customerId, productId);
        return ResponseBuilder.buildResponse(wishlist);
    }
}
