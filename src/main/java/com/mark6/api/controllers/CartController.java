package com.mark6.api.controllers;

import com.mark6.api.constants.URLMapping;
import com.mark6.api.helper.ResponseBuilder;
import com.mark6.api.model.Cart;
import com.mark6.api.model.ProductDetail;
import com.mark6.api.model.Response;
import com.mark6.api.services.CartService;
import com.mark6.api.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by mhussaa on 9/11/17.
 */
@Controller
@RequestMapping(URLMapping.CART_ROOT)
public class CartController {

    private CartService cartService;
    private ProductService productService;

    @Autowired
    public CartController(CartService cartService, ProductService productService) {
        this.cartService = cartService;
        this.productService = productService;
    }

    @RequestMapping(value = URLMapping.ADD_TO_CART, method = RequestMethod.GET)
    public @ResponseBody Response addToCart(@RequestParam(URLMapping.PARAM_CUSTOMER_ID) String customerId,
                                            @RequestParam(URLMapping.PARAM_PRODUCT_ID) String productId,
                                            @RequestParam(URLMapping.PARAM_QUANTITY) String quantity) {
        ProductDetail productDetail = productService.findProductByStyleCode(productId.split("::")[0]);
        Cart cartRef = cartService.addToCart(customerId, productId, Integer.parseInt(quantity), productDetail);
        return ResponseBuilder.buildResponse(cartRef);
    }

    @RequestMapping(value = URLMapping.GET_CART, method = RequestMethod.GET)
    public @ResponseBody Response getCart(@RequestParam(URLMapping.PARAM_CUSTOMER_ID) String customerId) {
        Cart cartRef = cartService.getCartRef(customerId);
        return ResponseBuilder.buildResponse(cartRef);
    }

    @RequestMapping(value = URLMapping.REMOVE_CART_ITEM, method = RequestMethod.GET)
    public @ResponseBody Response removeCartItem(@RequestParam(URLMapping.PARAM_CUSTOMER_ID) String customerId,
                                                 @RequestParam(URLMapping.PARAM_CART_ITEM_ID) String cartItemId,
                                                 @RequestParam(URLMapping.PARAM_CART_REF_ID) String cartRefId) {
        Cart cartRef = cartService.removeCartItem(cartRefId, customerId, cartItemId);
        return ResponseBuilder.buildResponse(cartRef);
    }

    @RequestMapping(value = URLMapping.UPDATE_QUANTITY, method = RequestMethod.GET)
    public @ResponseBody Response updateQuantity(@RequestParam(URLMapping.PARAM_CUSTOMER_ID) String customerId,
                                                 @RequestParam(URLMapping.PARAM_CART_ITEM_ID) String cartItemId,
                                                 @RequestParam(URLMapping.PARAM_QUANTITY) String quantity,
                                                 @RequestParam(URLMapping.PARAM_CART_REF_ID) String cartRefId) {
        Cart cartRef = cartService.updateQuantity(cartRefId, customerId, cartItemId, quantity);
        return ResponseBuilder.buildResponse(cartRef);
    }
}
