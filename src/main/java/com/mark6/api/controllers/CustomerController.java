package com.mark6.api.controllers;

import com.google.gson.Gson;
import com.mark6.api.constants.URLMapping;
import com.mark6.api.helper.ResponseBuilder;
import com.mark6.api.helper.Util;
import com.mark6.api.model.*;
import com.mark6.api.services.AccessTokenService;
import com.mark6.api.services.CustomerService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = URLMapping.CUSTOMER_ROOT)
@Log4j2
public class CustomerController {

    private CustomerService customerService;
    private AccessTokenService accessTokenService;

    @Autowired
    public CustomerController(CustomerService customerService, AccessTokenService accessTokenService) {
        this.customerService = customerService;
        this.accessTokenService = accessTokenService;
    }

    @RequestMapping(value = URLMapping.GET_CUSTOMER_BY_ID, method = RequestMethod.GET)
    public @ResponseBody Response getCustomerById(@PathVariable(name = URLMapping.PARAM_CUSTOMER_ID) String customerId) {
        if(Util.isNullOrEmpty(customerId)) {
            return ResponseBuilder.buildResponse(null);
        }

        Customer customer = customerService.getCustomerById(customerId);
        return ResponseBuilder.buildResponse(customer);
    }

    @RequestMapping(value = URLMapping.ROOT, method = RequestMethod.GET)
    public @ResponseBody Response getCustomerByEmail(@RequestParam(value = URLMapping.PARAM_EMAIL) String email) {
        if(Util.isNullOrEmpty(email)) {
            return ResponseBuilder.buildResponse(null);
        }
        Customer customer = customerService.getCustomerByEmail(email);
        return ResponseBuilder.buildResponse(customer);
    }

    @RequestMapping(value = URLMapping.CREATE_CUSTOMER, method = RequestMethod.POST)
    public @ResponseBody Response createCustomer(@RequestBody(required = false) Customer customer) {
        Customer createdCustomer = customerService.createCustomer(customer);
        return ResponseBuilder.buildResponse(createdCustomer);
    }

    @RequestMapping(value = URLMapping.GET_ACCESS_TOKEN, method = RequestMethod.GET)
    public @ResponseBody Response getAccessToken(@RequestParam(value = URLMapping.PARAM_CUSTOMER_ID) String customerId) {
        if(Util.isNullOrEmpty(customerId)) {
            return ResponseBuilder.buildResponse(null);
        }

        String accessToken = accessTokenService.getAccessToken(customerId);
        return ResponseBuilder.buildResponse(accessToken);
    }

    @RequestMapping(value = URLMapping.GET_VISITOR_ID, method = RequestMethod.GET)
    public @ResponseBody Response getVisitorId() {
        AccessToken accessToken = accessTokenService.getAccessToken();

        Map<String, String> tokens = new HashMap<>();
        tokens.put("visitorId", accessToken.getCustomerId());
        tokens.put("accessToken", accessToken.getValue());

        return ResponseBuilder.buildResponse(tokens);
    }

    @RequestMapping(value = URLMapping.LOGOUT, method = RequestMethod.GET)
    public @ResponseBody Response logout(@RequestParam(value = URLMapping.PARAM_CUSTOMER_ID) String customerId) {
        if(Util.isNullOrEmpty(customerId)) {
            return ResponseBuilder.buildResponse(null);
        }

        boolean isLogout = accessTokenService.logout(customerId);
        return ResponseBuilder.buildResponse(isLogout);
    }

    @RequestMapping(value = URLMapping.GET_CUSTOMER_DETAIL, method = RequestMethod.GET)
    public @ResponseBody Response getCustomerDetail(@RequestParam(value = URLMapping.PARAM_CUSTOMER_ID) String customerId) {
        if(Util.isNullOrEmpty(customerId)) {
            return ResponseBuilder.buildResponse(null);
        }

        CustomerDetail customer = customerService.getCustomerDetail(customerId);
        return ResponseBuilder.buildResponse(customer);
    }

    @RequestMapping(value = URLMapping.GET_ADDRESS_LIST, method = RequestMethod.GET)
    public @ResponseBody Response getAddressList(@RequestParam(value = URLMapping.PARAM_CUSTOMER_ID) String customerId) {
        if(Util.isNullOrEmpty(customerId)) {
            return ResponseBuilder.buildResponse(null);
        }

        List<Address> addressList = customerService.getAddressList(customerId);
        return ResponseBuilder.buildResponse(addressList);
    }

    @RequestMapping(value = URLMapping.SAVE_ADDRESS, method = RequestMethod.POST)
    public @ResponseBody Response saveAddress(@RequestBody Map<String, Object> saveAddressFormData) {
        String customerId = (String) saveAddressFormData.get(URLMapping.PARAM_CUSTOMER_ID);
        if(Util.isNullOrEmpty(customerId)) {
            return ResponseBuilder.buildResponse(null);
        }

        Address address = new Address().fromJSON(new Gson().toJson(saveAddressFormData));
        Address savedAddress = customerService.saveAddress(customerId, address);
        return ResponseBuilder.buildResponse(savedAddress);
    }

    @RequestMapping(value = URLMapping.UPDATE_CUSTOMER_DETAIL, method = RequestMethod.POST)
    public @ResponseBody Response updateCustomerDetail(@RequestBody Map<String, Object> customerFormData) {
        String customerId = (String) customerFormData.get(URLMapping.PARAM_CUSTOMER_ID);
        if(Util.isNullOrEmpty(customerId)) {
            return ResponseBuilder.buildResponse(null);
        }

        Customer customer = new Customer().fromJSON(new Gson().toJson(customerFormData));
        CustomerDetail customerDetail = customerService.updateCustomerDetail(customerId, customer);
        return ResponseBuilder.buildResponse(customerDetail);
    }

    @RequestMapping(value = URLMapping.DELETE_ADDRESS, method = RequestMethod.GET)
    public @ResponseBody Response deleteAddress(@RequestParam(value = URLMapping.PARAM_CUSTOMER_ID) String customerId,
                                                @RequestParam(value = URLMapping.PARAM_ADDRESS_ID) String addressId) {
        if(Util.isNullOrEmpty(customerId)) {
            return ResponseBuilder.buildResponse(null);
        }

        customerService.deleteAddress(customerId, addressId);
        Address address = new Address();
        address.setId(addressId);
        return ResponseBuilder.buildResponse(address);
    }

    @RequestMapping(value = URLMapping.SET_DEFAULT_ADDRESS, method = RequestMethod.GET)
    public @ResponseBody Response setDefaultAddress(@RequestParam(value = URLMapping.PARAM_CUSTOMER_ID) String customerId,
                                                    @RequestParam(value = URLMapping.PARAM_ADDRESS_ID) String addressId) {
        if(Util.isNullOrEmpty(customerId)) {
            return ResponseBuilder.buildResponse(null);
        }

        List<Address> addressList = customerService.setDefaultAddress(customerId, addressId);
        return ResponseBuilder.buildResponse(addressList);
    }
}
