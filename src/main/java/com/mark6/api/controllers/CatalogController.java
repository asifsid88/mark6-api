package com.mark6.api.controllers;

import com.mark6.api.constants.CatalogType;
import com.mark6.api.constants.URLMapping;
import com.mark6.api.helper.ResponseBuilder;
import com.mark6.api.model.Catalog;
import com.mark6.api.model.Response;
import com.mark6.api.services.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by mhussaa on 9/11/17.
 */
@Controller
@RequestMapping(URLMapping.CATALOG_ROOT)
public class CatalogController {

    private CatalogService catalogService;

    @Autowired
    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @RequestMapping(value = URLMapping.ROOT, method = RequestMethod.GET)
    public @ResponseBody Response getCatalogById(@RequestParam(value = URLMapping.PARAM_CATALOG_ID) String catalogId) {
        CatalogType catalogType = getCatalogType(catalogId);

        Catalog catalog = null;
        if(catalogType != null) {
            catalog = catalogService.getCatalog(catalogType);
        }

        return ResponseBuilder.buildResponse(catalog);
    }

    private CatalogType getCatalogType(String catalogId) {
        switch(catalogId) {
            case "browse":
                return CatalogType.BROWSE_PAGE;
            case "newlaunch":
                return CatalogType.NEWLY_LAUNCHED;
            default:
                return null;
        }
    }
}
