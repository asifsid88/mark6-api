package com.mark6.api.controllers;

import com.mark6.api.constants.URLMapping;
import com.mark6.api.helper.ResponseBuilder;
import com.mark6.api.model.*;
import com.mark6.api.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by mhussaa on 9/11/17.
 */
@Controller
@RequestMapping(URLMapping.PRODUCT_ROOT)
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(value = URLMapping.GET_PRODUCT_BY_ID, method = RequestMethod.GET)
    public @ResponseBody Response getProductById(@PathVariable(name = URLMapping.PARAM_PRODUCT_ID) String productId) {
        ProductDetail productDetail = productService.getProductById(productId);
        return ResponseBuilder.buildResponse(productDetail);
    }

    // TODO: Make it post
    @RequestMapping(value = URLMapping.CREATE_PRODUCT, method = RequestMethod.GET)
    public @ResponseBody Response createProduct() {
        //createProducts();
        return ResponseBuilder.buildResponse(Boolean.FALSE);
    }

    /**
     * TODO: Delete me
     */
    private void createProducts() {
        productService.createProduct(getProduct("M6TS4411BG", "Beige"));
        productService.createProduct(getProduct("M6TS4411OG", "Olive Green"));
        productService.createProduct(getProduct("M6TS4411SG", "Steel Grey"));

        productService.createProduct(getProduct("M6TS4412BG", "Beige"));
        productService.createProduct(getProduct("M6TS4412OG", "Olive Green"));
        productService.createProduct(getProduct("M6TS4412SG", "Steel Grey"));

        productService.createProduct(getProduct("M6TS4413BG", "Beige"));
        productService.createProduct(getProduct("M6TS4413OG", "Olive Green"));
        productService.createProduct(getProduct("M6TS4413SG", "Steel Grey"));

        productService.createProduct(getProduct("M6TS4414BG", "Beige"));
        productService.createProduct(getProduct("M6TS4414OG", "Olive Green"));
        productService.createProduct(getProduct("M6TS4414SG", "Steel Grey"));
    }

    private ProductDetail getProduct(String style, String color) {
        ProductDetail productDetail = new ProductDetail();

        productDetail.setTitle("Spider Round Neck " + color + " " + style);
        productDetail.setStyleCode(style);
        productDetail.setStock(16);

        Price price = new Price();
        price.setMrp(999);
        price.setSellingPrice(499);
        price.setOffer(50);
        productDetail.setPrice(price);

        List<Thumbnail> thumbnails = new LinkedList<>();
        Thumbnail thumbnail1 = new Thumbnail();
        thumbnail1.setImageUrl("/static/img/products/" + style + "/1.jpg");
        thumbnails.add(thumbnail1);

        Thumbnail thumbnail2 = new Thumbnail();
        thumbnail2.setImageUrl("/static/img/products/" + style + "/2.jpg");
        thumbnails.add(thumbnail2);

        Thumbnail thumbnail3 = new Thumbnail();
        thumbnail3.setImageUrl("/static/img/products/" + style + "/3.jpg");
        thumbnails.add(thumbnail3);

        Thumbnail thumbnail4 = new Thumbnail();
        thumbnail4.setImageUrl("/static/img/products/" + style + "/4.jpg");
        thumbnails.add(thumbnail4);

        productDetail.setThumbnails(thumbnails);


        Thumbnail thumbnail5 = new Thumbnail();
        thumbnail5.setImageUrl("/static/img/catalog/" + style + ".jpg");
        productDetail.setDefaultImage(thumbnail5);

        List<Size> sizes = new LinkedList<>();
        Size size1 = new Size();
        size1.setCount(10);
        size1.setValue("S");
        sizes.add(size1);

        Size size2 = new Size();
        size2.setCount(10);
        size2.setValue("M");
        sizes.add(size2);

        Size size3 = new Size();
        size3.setCount(10);
        size3.setValue("L");
        sizes.add(size3);

        Size size4 = new Size();
        size4.setCount(10);
        size4.setValue("XL");
        sizes.add(size4);

        productDetail.setSizes(sizes);
        productDetail.setActive(Boolean.TRUE);
        productDetail.setColor(color);
        productDetail.setDescription("This T-shirt from Mark6 is the perfect combination of style and comfort. Made from 100% cotton material, this T-shirt features a round neck with trendy flock print on the front.<ul><li>Brand name print</li><li>Round neck</li><li>Half sleeves</li><li>Cotton fabric</li><li>Regular fit</li></ul>Team this T-shirt with washed jeans and ankle boots for a jaunty look.");
        productDetail.setMaterial("<ul><li>100% Cotton</li><li>Warm machine wash</li><li>Do not bleach</li><li>Do not tumble dry</li><li>Warm iron</li><li>Dry cleanable</li></ul>");
        productDetail.setTshirtType("Round Neck");

        return productDetail;
    }
}
