package com.mark6.api.controllers;

import com.mark6.api.constants.URLMapping;
import com.mark6.api.helper.ResponseBuilder;
import com.mark6.api.model.Order;
import com.mark6.api.model.Response;
import com.mark6.api.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by mhussaa on 9/11/17.
 */
@Controller
@RequestMapping(URLMapping.ORDER_ROOT)
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @RequestMapping(value = URLMapping.GET_ORDER_DETAILS, method = RequestMethod.GET)
    public @ResponseBody Response getOrderDetails(@RequestParam(URLMapping.PARAM_ORDER_ID) String orderId) {
        Order order = orderService.getOrderDetails(orderId);
        return ResponseBuilder.buildResponse(order);
    }

    @RequestMapping(value = URLMapping.GET_ORDERS, method = RequestMethod.GET)
    public @ResponseBody Response getOrders(@RequestParam(URLMapping.PARAM_CUSTOMER_ID) String customerId) {
        List<Order> orderList = orderService.getAllOrdersByCustomerId(customerId);
        if(orderList == null) {
            orderList = new LinkedList<>();
        }

        return ResponseBuilder.buildResponse(orderList);
    }

}
