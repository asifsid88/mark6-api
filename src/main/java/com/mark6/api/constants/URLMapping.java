package com.mark6.api.constants;

/**
 * Created by mhussaa on 9/2/17.
 */
public interface URLMapping {
    String ROOT = "/";

    // Mapping for Product API
    String PRODUCT_ROOT = "product/";
    String GET_PRODUCT_BY_ID = "{productId}";
    String CREATE_PRODUCT = "create";

    // Mapping for Catalog API
    String CATALOG_ROOT = "catalog/";

    // Mapping for Customer API
    String CUSTOMER_ROOT = "customer/";
    String GET_CUSTOMER_BY_ID = "{customerId}";
    String REGISTER_CUSTOMER = "register";
    String CREATE_CUSTOMER = "create";
    String GET_ACCESS_TOKEN = "accesstoken";
    String GET_VISITOR_ID = "visitorid";
    String LOGOUT = "logout";
    String GET_CUSTOMER_DETAIL = "detail";
    String GET_ADDRESS_LIST = "addresses";
    String SAVE_ADDRESS = "saveAddress";
    String DELETE_ADDRESS = "deleteAddress";
    String SET_DEFAULT_ADDRESS = "setDefaultAddress";
    String UPDATE_CUSTOMER_DETAIL = "updateDetail";
    String PLACE_ORDER = "placeOrder";

    // Mapping for Cart API
    String CART_ROOT = "cart/";
    String ADD_TO_CART = "add";
    String GET_CART = "home";
    String REMOVE_CART_ITEM = "remove";
    String UPDATE_QUANTITY = "updateqty";

    // Mapping for Wishlit API
    String WISHLIST_ROOT = "wishlist/";
    String ADD_TO_WISHLIST = "add";
    String GET_WISHLIST = "home";
    String REMOVE_WISHLIST_ITEM = "remove";
    String MOVE_TO_CART = "move";

    // Mapping for Checkout API
    String CHECKOUT_ROOT = "checkout/";
    String CHECKOUT_INIT = "init";
    String CHECKOUT_SELECT_ADDRESS = "selectAddress";

    // Mapping for Order API
    String ORDER_ROOT = "order/";
    String GET_ORDER_DETAILS = "details";
    String GET_ORDERS = "all";

    // Path Params and Query Strings
    String PARAM_PRODUCT_ID = "productId";
    String PARAM_CUSTOMER_ID = "customerId";
    String PARAM_CATALOG_ID = "catalogId";
    String PARAM_EMAIL = "email";
    String PARAM_QUANTITY = "quantity";
    String PARAM_CART_ITEM_ID = "cartItemId";
    String PARAM_CART_REF_ID = "cartRefId";
    String PARAM_ADDRESS_ID = "addressId";
    String PARAM_CHECKOUT_OBJ = "checkout";
    String PARAM_ORDER_ID = "orderId";
}
