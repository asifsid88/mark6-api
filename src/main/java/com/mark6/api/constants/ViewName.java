package com.mark6.api.constants;

/**
 * Created by mhussaa on 9/2/17.
 */
public interface ViewName {
    String INDEX = "index";
    String PRODUCT = "product";
}
