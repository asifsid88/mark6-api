package com.mark6.api.constants;

import org.springframework.data.domain.Sort;

/**
 * Created by mhussaa on 9/13/17.
 */
public enum CatalogType {

    NEWLY_LAUNCHED("Newly Launched", null),
    BROWSE_PAGE("Browse Page", null);

    private String catalogName;
    private Sort sortingCriteria;

    CatalogType(String catalogName, Sort sortingCriteria) {
        this.catalogName = catalogName;
        this.sortingCriteria = sortingCriteria;
    }

    public String getCatalogName() {
        return this.catalogName;
    }

    public Sort getSortingCriteria() {
        return this.sortingCriteria;
    }

    @Override
    public String toString() {
        return this.catalogName;
    }
}

