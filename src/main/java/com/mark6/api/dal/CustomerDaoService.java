package com.mark6.api.dal;

import com.mark6.api.helper.IDType;
import com.mark6.api.helper.Util;
import com.mark6.api.model.*;
import com.mark6.api.repositories.CustomerDetailRepository;
import com.mark6.api.repositories.CustomerRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
@Log4j2
public class CustomerDaoService {

    private CustomerRepository customerRepository;
    private CustomerDetailRepository customerDetailRepository;

    @Autowired
    public CustomerDaoService(CustomerRepository customerRepository, CustomerDetailRepository customerDetailRepository) {
        this.customerRepository = customerRepository;
        this.customerDetailRepository = customerDetailRepository;
    }

    public Customer getCustomerById(String customerId) {
        return customerRepository.findOne(customerId);
    }

    public Customer getCustomerByEmail(String email) {
        return customerRepository.findByEmail(email);
    }

    public CustomerDetail getCustomerDetail(String customerId) {
        return customerDetailRepository.findOne(customerId);
    }

    public Customer createCustomer(Customer customer) {
        customer = customerRepository.save(customer);
        saveCustomerDetail(createCustomerDetail(customer));
        return customer;
    }

    public Customer saveCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public CustomerDetail saveCustomerDetail(CustomerDetail customerDetail) {
        return customerDetailRepository.save(customerDetail);
    }

    public List<Address> getAddressList(String customerId) {
        CustomerDetail customerDetail = getCustomerDetail(customerId);
        return customerDetail.getAddressList();
    }

    private CustomerDetail createCustomerDetail(Customer customer) {
        CustomerDetail customerDetail = new CustomerDetail();
        customerDetail.setId(customer.getId());
        customerDetail.setName(customer.getName());
        customerDetail.setEmail(customer.getEmail());
        customerDetail.setPictureUrl(customer.getPictureUrl());
        customerDetail.setSocialType(Social.FACEBOOK);
        customerDetail.setSocialId(customer.getSocialId());
        customerDetail.setAccountCreationDate(new Date());

        // TODO: Remove me when AddressService is ready
        customerDetail.setAddressList(getAddressList());

        return customerDetail;
    }


    /***
     * TODO: Remove me
     */
    private List<Address> getAddressList() {
        List<Address> addressList = new LinkedList<>();
        addressList.add(getAddress(10));
        addressList.add(getAddress(20));

        return addressList;
    }

    private Address getAddress(int x) {
        Address address = new Address();
        address.setAddressType(AddressType.HOME);
        address.setId(Util.createId(IDType.ADDRESS));
        address.setContactPerson("Asif");
        address.setPhone("9967669199");
        address.setAlternatePhone("");
        address.setStreet1("9, Kazi Street, Bora Building");
        address.setStreet2("2nd Floor, Room No. 202, Null bazar");
        address.setLandmark("Null bazar polic chowki");
        address.setCity("Mumbai");
        address.setState("Maharashtra");
        address.setCountry("India");
        address.setPincode(400003);
        address.setDefault(Boolean.FALSE);

        return address;
    }
}
