package com.mark6.api.dal;

import com.mark6.api.model.ProductDetail;
import com.mark6.api.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mhussaa on 9/13/17.
 */
@Service
public class ProductDaoService {

    private ProductRepository productRepository;

    @Autowired
    public ProductDaoService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ProductDetail save(ProductDetail productDetail) {
        return productRepository.save(productDetail);
    }

    public ProductDetail updateProduct(ProductDetail productDetail) {
        // TODO: get the productDetail, update the field and then save it

        return productRepository.save(productDetail);
    }

    public void delete(ProductDetail productDetail) {
        productRepository.delete(productDetail);
    }

    public ProductDetail getProductById(String productId) {
        return productRepository.findOne(productId);
    }

    public boolean isProductExists(ProductDetail productDetail) {
        return productRepository.exists(productDetail.getId());
    }

    public List<ProductDetail> findAllProduct() {
        return productRepository.findAll();
    }

    public List<ProductDetail> findAllProductInPage(int pageNumber, int numberOfItemsPerPage) {
        // TODO: You can also provide sort criteria while getting the data in pages
        Pageable pageable = new PageRequest(pageNumber, numberOfItemsPerPage);
        Page<ProductDetail> page = productRepository.findAll(pageable);

        return page.getContent();
    }

    public ProductDetail findProductByStyleCode(String styleCode) {
        return productRepository.findByStyleCode(styleCode);
    }
}
