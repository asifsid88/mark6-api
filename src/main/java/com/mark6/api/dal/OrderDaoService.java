package com.mark6.api.dal;

import com.mark6.api.model.Order;
import com.mark6.api.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mhussaa on 9/13/17.
 */
@Service
public class OrderDaoService {

    private OrderRepository orderRepository;

    @Autowired
    public OrderDaoService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public void saveOrder(Order order) {
        orderRepository.save(order);
    }

    public Order getOrderById(String orderId) {
        return orderRepository.findOne(orderId);
    }

    public List<Order> getAllOrdersByCustomerId(String customerId) {
        return orderRepository.findByCustomerId(customerId);
    }
}
