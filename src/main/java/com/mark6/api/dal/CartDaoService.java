package com.mark6.api.dal;

import com.mark6.api.helper.IDType;
import com.mark6.api.helper.Util;
import com.mark6.api.model.Cart;
import com.mark6.api.repositories.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;

/**
 * Created by mhussaa on 9/13/17.
 */
@Service
public class CartDaoService {

    private CartRepository cartRepository;

    @Autowired
    public CartDaoService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    public Cart updateCart(Cart cart) {
        return cartRepository.save(cart);
    }

    public Cart getCartRefId(String customerId) {
        return cartRepository.findByCustomerIdAndIsTransientFalse(customerId);
    }

    public Cart getTransientCart(String customerId) {
        return cartRepository.findByCustomerIdAndIsTransientTrue(customerId);
    }

    public Cart getCart(String cartRefId) {
        return cartRepository.findOne(cartRefId);
    }

    public Cart createCart(String customerId) {
        return cartRepository.save(getNewCart(customerId, Boolean.FALSE));
    }

    public Cart createTransientCart(String customerId) {
        return cartRepository.save(getNewCart(customerId, Boolean.TRUE));
    }

    private Cart getNewCart(String customerId, boolean isTransient) {
        Cart cart = new Cart();
        cart.setId(Util.createId(IDType.CART));
        cart.setTransient(isTransient);
        cart.setCustomerId(customerId);
        cart.setCartItems(new LinkedList<>());

        return cart;
    }

    public void deleteCart(String cartRefId) {
        cartRepository.delete(cartRefId);
    }
}
