package com.mark6.api.dal;

import com.mark6.api.constants.CatalogType;
import com.mark6.api.helper.ModelHelper;
import com.mark6.api.model.Catalog;
import com.mark6.api.model.ProductDetail;
import com.mark6.api.repositories.CatalogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mhussaa on 9/13/17.
 */
@Service
public class CatalogDaoService {

    private CatalogRepository catalogRepository;
    private ModelHelper modelHelper;

    @Autowired
    public CatalogDaoService(CatalogRepository catalogRepository, ModelHelper modelHelper) {
        this.catalogRepository = catalogRepository;
        this.modelHelper = modelHelper;
    }

    public Catalog getCatalog(CatalogType catalogType) {
        List<ProductDetail> productDetails = catalogRepository.findAll(catalogType.getSortingCriteria());
        return modelHelper.createCatalog(productDetails, catalogType.getCatalogName());
    }
}
