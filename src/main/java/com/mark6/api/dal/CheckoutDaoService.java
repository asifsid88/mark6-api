package com.mark6.api.dal;

import com.mark6.api.model.Checkout;
import com.mark6.api.repositories.CheckoutRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mhussaa on 9/13/17.
 */
@Service
public class CheckoutDaoService {

    private CheckoutRepository checkoutRepository;

    @Autowired
    public CheckoutDaoService(CheckoutRepository checkoutRepository) {
        this.checkoutRepository = checkoutRepository;
    }

    public Checkout init(Checkout checkout) {
        return checkoutRepository.save(checkout);
    }

    public void deleteCheckout(String checkoutId) {
        checkoutRepository.delete(checkoutId);
    }
}
