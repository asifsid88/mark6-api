package com.mark6.api.dal;

import com.mark6.api.helper.IDType;
import com.mark6.api.helper.Util;
import com.mark6.api.model.Wishlist;
import com.mark6.api.repositories.WishlistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;

/**
 * Created by mhussaa on 9/13/17.
 */
@Service
public class WishlistDaoService {

    private WishlistRepository wishlistRepository;

    @Autowired
    public WishlistDaoService(WishlistRepository wishlistRepository) {
        this.wishlistRepository = wishlistRepository;
    }

    public Wishlist updateWishlist(Wishlist wishlist) {
        return wishlistRepository.save(wishlist);
    }

    public Wishlist getWishlist(String customerId) {
        Wishlist wishlist = wishlistRepository.findByCustomerId(customerId);
        if(Util.isNullOrEmpty(wishlist) || Util.isNullOrEmpty(wishlist.getId())) {
            wishlist = createWishlist(customerId);
        }

        return wishlist;
    }

    public Wishlist createWishlist(String customerId) {
        Wishlist wishlist = new Wishlist();
        wishlist.setId(Util.createId(IDType.WISHLIST));
        wishlist.setCustomerId(customerId);
        wishlist.setCartItems(new LinkedList<>());

        return wishlistRepository.save(wishlist);
    }
}
