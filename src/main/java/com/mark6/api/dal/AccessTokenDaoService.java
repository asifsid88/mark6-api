package com.mark6.api.dal;

import com.mark6.api.model.AccessToken;
import com.mark6.api.repositories.AccessTokenRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Service
@Log4j2
public class AccessTokenDaoService {

    private AccessTokenRepository accessTokenRepository;

    @Autowired
    public AccessTokenDaoService(AccessTokenRepository accessTokenRepository) {
        this.accessTokenRepository = accessTokenRepository;
    }

    public AccessToken getAccessToken(String customerId) {
        return accessTokenRepository.findByCustomerId(customerId);
    }

    public boolean logout(String customerId) {
        try {
            accessTokenRepository.deleteByCustomerId(customerId);
            return true;
        } catch(Exception e) {
            log.error("Exception while deleting access token by customerId: {}, Exception: {}", customerId, e);
            return false;
        }
    }

    public AccessToken createAccessToken(String customerId) {
        AccessToken accessToken = new AccessToken();
        accessToken.setCustomerId(customerId);
        accessToken.setValue(generateToken());
        accessToken.setExpiry(getExpiryDate());

        return accessTokenRepository.save(accessToken);
    }

    public boolean isAccessTokenExpired(AccessToken accessToken) {
        Date today = new Date();
        Date tokenDate = accessToken.getExpiry();

        return today.compareTo(tokenDate) > 0;
    }

    private String generateToken() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    private Date getExpiryDate() {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.add(Calendar.DATE, 10);

        return calendar.getTime();
    }
}
