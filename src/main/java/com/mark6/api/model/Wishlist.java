package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


/**
 * Created by mhussaa on 9/10/17.
 */
@Data
@Document(collection = "wishlist")
public class Wishlist implements IModel {

    @Id
    private String id;
    private String customerId;
    private List<CartItem> cartItems;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public Wishlist fromJSON(String data) {
        return new Gson().fromJson(data, Wishlist.class);
    }
}
