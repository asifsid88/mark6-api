package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Data
@Document(collection = "customer_detail")
public class CustomerDetail implements IModel {

    @Id
    private String id;

    private String name;
    private String email;
    private String phone;
    private String pictureUrl;
    private Date accountCreationDate;
    private Social socialType;
    private String socialId;
    private String gender;
    private List<Address> addressList = new LinkedList<>();

    @Transient
    private List<String> wishlist = new LinkedList<>();

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public CustomerDetail fromJSON(String data) {
        return new Gson().fromJson(data, CustomerDetail.class);
    }
}
