package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


/**
 * Created by mhussaa on 9/10/17.
 */
@Data
@Document(collection = "cart")
public class Cart implements IModel {

    @Id
    private String id;
    private String customerId;
    private List<CartItem> cartItems;
    private boolean isTransient;

    @Transient
    private AmountPayable amountPayable;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public Cart fromJSON(String data) {
        return new Gson().fromJson(data, Cart.class);
    }
}
