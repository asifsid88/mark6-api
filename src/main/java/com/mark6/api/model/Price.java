package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;

/**
 * Created by mhussaa on 9/11/17.
 */
@Data
public class Price implements IModel {

    private int mrp;
    private int sellingPrice;
    private int offer;
    private int deliveryCharge;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public Price fromJSON(String data) {
        return new Gson().fromJson(data, Price.class);
    }
}
