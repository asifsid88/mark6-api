package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "customer")
public class Customer implements IModel {

    @Id
    private String id;

    private String socialId;
    private String name;
    private String email;
    private String phone;
    private String gender;
    private String pictureUrl;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public Customer fromJSON(String data) {
        return new Gson().fromJson(data, Customer.class);
    }
}
