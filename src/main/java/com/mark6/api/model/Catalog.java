package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;

import java.util.List;


/**
 * Created by mhussaa on 9/10/17.
 */
@Data
public class Catalog implements IModel {

    private String name;
    private List<Product> products;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public Catalog fromJSON(String data) {
        return new Gson().fromJson(data, Catalog.class);
    }
}
