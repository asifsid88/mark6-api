package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;

@Data
public class CartItem implements IModel {

    private String m6sin;
    private String productId;
    private String styleCode;

    private String color;
    private String size;
    private Price price;
    private String title;
    private String imageUrl;
    private int quantity;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public CartItem fromJSON(String data) {
        return new Gson().fromJson(data, CartItem.class);
    }
}
