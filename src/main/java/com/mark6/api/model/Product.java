package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;

import java.util.List;


/**
 * Created by mhussaa on 9/10/17.
 */
@Data
public class Product implements IModel {

    private String id;
    private String title;
    private Price price;
    private boolean isActive;
    private List<Size> sizes;
    private Thumbnail defaultImage;
    private String styleCode;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public Product fromJSON(String data) {
        return new Gson().fromJson(data, Product.class);
    }
}
