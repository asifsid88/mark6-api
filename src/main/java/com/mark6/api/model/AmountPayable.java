package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;

@Data
public class AmountPayable implements IModel {

    private int deliveryCharge;
    private int totalCost;
    private int totalSaving;
    private int totalItemCount;
    private int totalAmountPayable;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public AmountPayable fromJSON(String data) {
        return new Gson().fromJson(data, AmountPayable.class);
    }
}
