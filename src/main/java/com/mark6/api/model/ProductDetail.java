package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


/**
 * Created by mhussaa on 9/10/17.
 */
@Data
@Document(collection = "product")
public class ProductDetail implements IModel {

    @Id
    private String id;
    private String title;
    private int stock;
    private Price price;
    private boolean isActive;
    private List<Size> sizes;
    private String styleCode;
    private String description;
    private String material;
    private String tshirtType;
    private String color;
    private List<Thumbnail> thumbnails;
    private Thumbnail defaultImage;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public ProductDetail fromJSON(String data) {
        return new Gson().fromJson(data, ProductDetail.class);
    }
}
