package com.mark6.api.model;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by mhussaa on 9/10/17.
 */
@Data
public class Response implements Serializable {
    private int status;
    private String message;
    private Object data;
}