package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * Created by mhussaa on 9/10/17.
 */
@Data
@Document(collection = "checkout")
public class Checkout implements IModel {

    @Id
    private String id;
    private String customerId;
    private String cartRefId;
    private Address deliveryAddress;

    @Transient
    private String name;

    @Transient
    private String email;

    @Transient
    private Cart orderSummary;

    @Transient
    private PaymentMethod paymentMethod;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public Checkout fromJSON(String data) {
        return new Gson().fromJson(data, Checkout.class);
    }
}
