package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by mhussaa on 9/10/17.
 */
@Data
@Document(collection = "order")
public class Order implements IModel {

    @Id
    private String id;
    private String customerId;
    private Address deliveryAddress;
    private String name;
    private String email;
    private List<CartItem> cartItems;
    private String orderCreationDate;
    private PaymentMethod paymentMethod;
    private AmountPayable amountPayable;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public Order fromJSON(String data) {
        return new Gson().fromJson(data, Order.class);
    }
}
