package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;

/**
 * Created by mhussaa on 9/11/17.
 */
@Data
public class PaymentMethod implements IModel {

    private String mode;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public PaymentMethod fromJSON(String data) {
        return new Gson().fromJson(data, PaymentMethod.class);
    }
}
