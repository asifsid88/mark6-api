package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;

/**
 * Created by mhussaa on 9/11/17.
 */
@Data
public class Size implements IModel {

    private String id;
    private int count;
    private String value;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public Size fromJSON(String data) {
        return new Gson().fromJson(data, Size.class);
    }
}
