package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection = "access_token")
public class AccessToken implements IModel {

    @Id
    private String id;

    private String value;
    private String customerId;
    private Date expiry;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public AccessToken fromJSON(String data) {
        return new Gson().fromJson(data, AccessToken.class);
    }
}
