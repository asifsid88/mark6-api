package com.mark6.api.model;

import java.io.Serializable;

/**
 * Created by mhussaa on 9/10/17.
 */
public interface IModel extends Serializable {
    String toJSON(IModel model);
    IModel fromJSON(String data);
}