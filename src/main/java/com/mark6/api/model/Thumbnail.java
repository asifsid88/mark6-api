package com.mark6.api.model;

import com.google.gson.Gson;
import lombok.Data;

/**
 * Created by mhussaa on 9/11/17.
 */
@Data
public class Thumbnail implements IModel {

    private String imageUrl;
    private String compressedImageUrl;

    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    public Thumbnail fromJSON(String data) {
        return new Gson().fromJson(data, Thumbnail.class);
    }
}
