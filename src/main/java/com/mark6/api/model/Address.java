package com.mark6.api.model;

import com.google.gson.Gson;
import com.mark6.api.helper.Util;
import lombok.Data;

@Data
public class Address implements IModel {
    private String id;
    private String contactPerson;
    private String phone;
    private String alternatePhone;
    private AddressType addressType;

    private String street1;
    private String street2;
    private String landmark;
    private String state;
    private String city;
    private String country = "India";
    private int pincode;
    private boolean isDefault;
    private String creationDate;
    private String updateDate;

    @Override
    public String toJSON(IModel model) {
        return new Gson().toJson(model);
    }

    @Override
    public Address fromJSON(String data) {
        return new Gson().fromJson(data, Address.class);
    }

    @Override
    public String toString() {
        StringBuilder address = new StringBuilder(street1);
        if(!Util.isNullOrEmpty(street2)) {
            address.append(", ");
        }

        address.append(street2)
                .append(", ")
                .append(landmark)
                .append(", ")
                .append(city)
                .append(", ")
                .append(state)
                .append(", ")
                .append(country)
                .append(" - ")
                .append(pincode);

        return address.toString();
    }
}
