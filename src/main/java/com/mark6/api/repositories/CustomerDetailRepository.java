package com.mark6.api.repositories;


import com.mark6.api.model.CustomerDetail;
import org.springframework.data.repository.CrudRepository;

public interface CustomerDetailRepository extends CrudRepository<CustomerDetail, String>{
}
