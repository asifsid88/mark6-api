package com.mark6.api.repositories;

import com.mark6.api.model.AccessToken;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by mhussaa on 9/13/17.
 */
public interface AccessTokenRepository extends MongoRepository<AccessToken, String> {
    AccessToken findByCustomerId(String customerId);
    void deleteByCustomerId(String customerId);
}
