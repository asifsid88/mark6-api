package com.mark6.api.repositories;

import com.mark6.api.model.Wishlist;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by mhussaa on 9/13/17.
 */
public interface WishlistRepository extends MongoRepository<Wishlist, String> {
    Wishlist findByCustomerId(String customerId);
}
