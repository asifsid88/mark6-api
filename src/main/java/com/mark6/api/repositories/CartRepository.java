package com.mark6.api.repositories;

import com.mark6.api.model.Cart;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by mhussaa on 9/13/17.
 */
public interface CartRepository extends MongoRepository<Cart, String> {
    Cart findByCustomerIdAndIsTransientFalse(String customerId);
    Cart findByCustomerIdAndIsTransientTrue(String customerId);
}
