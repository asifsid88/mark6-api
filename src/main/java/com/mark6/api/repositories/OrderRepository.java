package com.mark6.api.repositories;

import com.mark6.api.model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by mhussaa on 9/13/17.
 */
public interface OrderRepository extends MongoRepository<Order, String> {
    List<Order> findByCustomerId(String customerId);
}
