package com.mark6.api.repositories;


import com.mark6.api.model.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, String>{
    Customer findByEmail(String email);
}
