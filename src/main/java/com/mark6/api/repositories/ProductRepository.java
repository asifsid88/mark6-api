package com.mark6.api.repositories;

import com.mark6.api.model.ProductDetail;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by mhussaa on 9/13/17.
 */
public interface ProductRepository extends MongoRepository<ProductDetail, String> {
    ProductDetail findByStyleCode(String styleCode);
}
