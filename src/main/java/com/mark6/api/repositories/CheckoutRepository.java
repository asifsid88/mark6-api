package com.mark6.api.repositories;

import com.mark6.api.model.Checkout;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by mhussaa on 9/13/17.
 */
public interface CheckoutRepository extends MongoRepository<Checkout, String> {
    Checkout findByCustomerId(String customerId);
}
