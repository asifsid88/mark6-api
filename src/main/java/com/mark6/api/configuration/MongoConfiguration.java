package com.mark6.api.configuration;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by mhussaa on 9/12/17.
 */
@Configuration
@EnableMongoRepositories({"com.mark6.api.repositories"})
public class MongoConfiguration {

    private static final String DATABASE = "mongo.database";
    private static final String MONGO_CLIENT_URI = "mongo.clienturi";

    @Autowired
    private Environment environment;

    @Bean
    public Mongo mongo() throws Exception {
        MongoClientURI clientURI = new MongoClientURI(environment.getProperty(MONGO_CLIENT_URI));
        return new MongoClient(clientURI);
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongo(), environment.getProperty(DATABASE));
    }
}