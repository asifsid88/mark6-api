package com.mark6.api.helper;

public enum IDType {
    CHECKOUT,
    ADDRESS,
    WISHLIST,
    CART
}
