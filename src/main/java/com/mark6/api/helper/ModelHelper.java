package com.mark6.api.helper;

import com.mark6.api.model.*;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by mhussaa on 9/13/17.
 */
@Component
public class ModelHelper {

    public Catalog createCatalog(List<ProductDetail> productDetails, String catalogName) {
        Catalog catalog = new Catalog();

        List<Product> products = new LinkedList<>();
        for(ProductDetail productDetail : productDetails) {
            products.add(createProduct(productDetail));
        }

        catalog.setProducts(products);
        catalog.setName(catalogName);

        return catalog;
    }

    public Product createProduct(ProductDetail productDetail) {
        Product product = new Product();
        product.setId(productDetail.getId());
        product.setTitle(productDetail.getTitle());
        product.setPrice(productDetail.getPrice());
        product.setActive(productDetail.isActive());
        product.setSizes(productDetail.getSizes());
        product.setDefaultImage(productDetail.getDefaultImage());
        product.setStyleCode(productDetail.getStyleCode());

        return product;
    }

    public void addCartItem(Cart cartRef, ProductDetail productDetail, String productId, int quantity) {
        CartItem cartItem = createCartItem(productDetail,
                                            productId.replaceAll("::", ""),
                                            productId.split("::")[1],
                                            quantity);
        cartRef.getCartItems().add(cartItem);
    }

    public void addWishlistItem(Wishlist wishlist, ProductDetail productDetail, String productId) {
        CartItem cartItem = createCartItem(productDetail, "", "", 0);
        wishlist.getCartItems().add(cartItem);
    }

    public CartItem createCartItem(ProductDetail productDetail, String m6sin, String size, int quantity) {
        CartItem cartItem = new CartItem();
        cartItem.setM6sin(m6sin);
        cartItem.setProductId(productDetail.getId());
        cartItem.setStyleCode(productDetail.getStyleCode());
        cartItem.setSize(size);
        cartItem.setColor(productDetail.getColor());
        cartItem.setPrice(productDetail.getPrice());
        cartItem.setTitle(productDetail.getTitle());
        cartItem.setImageUrl(productDetail.getThumbnails().get(0).getImageUrl());
        cartItem.setQuantity(quantity);

        return cartItem;
    }

    public AmountPayable getAmountPayable(List<CartItem> cartItems) {
        AmountPayable amountPayable = new AmountPayable();
        int deliveryCharge = 0;
        int totalCost = 0;
        int totalSaving = 0;
        int totalItemCount = 0;

        for(CartItem cartItem : cartItems) {
            Price price = cartItem.getPrice();
            int quantity = cartItem.getQuantity();

            deliveryCharge += 100; // price.getDeliveryCharge(); // Temporary
            totalItemCount += quantity;
            totalCost += (quantity * price.getSellingPrice());

            //totalCost += (quantity * price.getDeliveryCharge()); // TODO: If delivery charge is per piece basis

            totalSaving += quantity * (price.getMrp() - price.getSellingPrice());
        }

        amountPayable.setDeliveryCharge(deliveryCharge);
        amountPayable.setTotalCost(totalCost);
        amountPayable.setTotalAmountPayable(deliveryCharge + totalCost);
        amountPayable.setTotalSaving(totalSaving);
        amountPayable.setTotalItemCount(totalItemCount);

        return amountPayable;
    }

    public Checkout createCheckoutObject(Cart cart, Customer customer) {
        Checkout checkout = new Checkout();
        checkout.setId(Util.createId(IDType.CHECKOUT));
        checkout.setCustomerId(cart.getCustomerId());
        checkout.setCartRefId(cart.getId());
        checkout.setOrderSummary(cart);
        checkout.setEmail(customer.getEmail());
        checkout.setName(customer.getName());

        return checkout;
    }

    /**
     * Merges c2 in c1. Update c1 with non-null properties of c2
     * @param c1 Base customer
     * @param c2 Updated customer
     * @return Merged Customer having non-null properties of c2
     */
    public Customer mergeCustomer(Customer c1, Customer c2) {
        Customer mergedCustomer = new Customer();
        mergedCustomer.setId(c1.getId());

        if(Util.isNullOrEmpty(c2.getSocialId())) {
            mergedCustomer.setSocialId(c1.getSocialId());
        } else {
            mergedCustomer.setSocialId(c2.getSocialId());
        }

        if(Util.isNullOrEmpty(c2.getName())) {
            mergedCustomer.setName(c1.getName());
        } else {
            mergedCustomer.setName(c2.getName());
        }

        if(Util.isNullOrEmpty(c2.getEmail())) {
            mergedCustomer.setEmail(c1.getEmail());
        } else {
            mergedCustomer.setEmail(c2.getEmail());
        }

        if(Util.isNullOrEmpty(c2.getPhone())) {
            mergedCustomer.setPhone(c1.getPhone());
        } else {
            mergedCustomer.setPhone(c2.getPhone());
        }

        if(Util.isNullOrEmpty(c2.getGender())) {
            mergedCustomer.setGender(c1.getGender());
        } else {
            mergedCustomer.setGender(c2.getGender());
        }

        if(Util.isNullOrEmpty(c2.getPictureUrl())) {
            mergedCustomer.setPictureUrl(c1.getPictureUrl());
        } else {
            mergedCustomer.setPictureUrl(c2.getPictureUrl());
        }

        return mergedCustomer;
    }

    public CustomerDetail updateCustomerDetail(CustomerDetail c1, Customer c2) {
        CustomerDetail mergedCustomerDetail = new CustomerDetail();
        mergedCustomerDetail.setId(c1.getId());

        if(Util.isNullOrEmpty(c2.getSocialId())) {
            mergedCustomerDetail.setSocialId(c1.getSocialId());
        } else {
            mergedCustomerDetail.setSocialId(c2.getSocialId());
        }

        if(Util.isNullOrEmpty(c2.getName())) {
            mergedCustomerDetail.setName(c1.getName());
        } else {
            mergedCustomerDetail.setName(c2.getName());
        }

        if(Util.isNullOrEmpty(c2.getEmail())) {
            mergedCustomerDetail.setEmail(c1.getEmail());
        } else {
            mergedCustomerDetail.setEmail(c2.getEmail());
        }

        if(Util.isNullOrEmpty(c2.getPhone())) {
            mergedCustomerDetail.setPhone(c1.getPhone());
        } else {
            mergedCustomerDetail.setPhone(c2.getPhone());
        }

        if(Util.isNullOrEmpty(c2.getGender())) {
            mergedCustomerDetail.setGender(c1.getGender());
        } else {
            mergedCustomerDetail.setGender(c2.getGender());
        }

        if(Util.isNullOrEmpty(c2.getPictureUrl())) {
            mergedCustomerDetail.setPictureUrl(c1.getPictureUrl());
        } else {
            mergedCustomerDetail.setPictureUrl(c2.getPictureUrl());
        }

        return mergedCustomerDetail;
    }

    public CustomerDetail mergeCustomerDetail(CustomerDetail c1, CustomerDetail c2) {
        CustomerDetail mergedCustomerDetail = new CustomerDetail();
        mergedCustomerDetail.setId(c1.getId());

        if(Util.isNullOrEmpty(c2.getSocialId())) {
            mergedCustomerDetail.setSocialId(c1.getSocialId());
        } else {
            mergedCustomerDetail.setSocialId(c2.getSocialId());
        }

        if(Util.isNullOrEmpty(c2.getSocialType())) {
            mergedCustomerDetail.setSocialType(c1.getSocialType());
        } else {
            mergedCustomerDetail.setSocialType(c2.getSocialType());
        }

        if(Util.isNullOrEmpty(c2.getName())) {
            mergedCustomerDetail.setName(c1.getName());
        } else {
            mergedCustomerDetail.setName(c2.getName());
        }

        if(Util.isNullOrEmpty(c2.getEmail())) {
            mergedCustomerDetail.setEmail(c1.getEmail());
        } else {
            mergedCustomerDetail.setEmail(c2.getEmail());
        }

        if(Util.isNullOrEmpty(c2.getPhone())) {
            mergedCustomerDetail.setPhone(c1.getPhone());
        } else {
            mergedCustomerDetail.setPhone(c2.getPhone());
        }

        if(Util.isNullOrEmpty(c2.getGender())) {
            mergedCustomerDetail.setGender(c1.getGender());
        } else {
            mergedCustomerDetail.setGender(c2.getGender());
        }

        if(Util.isNullOrEmpty(c2.getPictureUrl())) {
            mergedCustomerDetail.setPictureUrl(c1.getPictureUrl());
        } else {
            mergedCustomerDetail.setPictureUrl(c2.getPictureUrl());
        }

        return mergedCustomerDetail;
    }

    public Order createOrderFromCheckout(Checkout checkout) {
        Order order = new Order();
        order.setId(checkout.getId());
        order.setCustomerId(checkout.getCustomerId());
        order.setDeliveryAddress(checkout.getDeliveryAddress());
        order.setName(checkout.getName());
        order.setEmail(checkout.getEmail());
        order.setCartItems(checkout.getOrderSummary().getCartItems());
        order.setAmountPayable(checkout.getOrderSummary().getAmountPayable());
        order.setPaymentMethod(checkout.getPaymentMethod());
        order.setOrderCreationDate(Util.getCurrentDate(DateFormat.DATE_TIME_DAY));

        return order;
    }
}
