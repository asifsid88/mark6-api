package com.mark6.api.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mhussaa on 9/10/17.
 */
public final class Util {

    public static String getCurrentDate(DateFormat dateFormat) {
        Calendar calendar = Calendar.getInstance();
        return  new SimpleDateFormat(dateFormat.getFormat()).format(calendar.getTime());
    }

    public static boolean isNullOrEmpty(Object obj) {
        if(obj == null) return true;

        if(obj instanceof String) {
            String val = String.valueOf(obj);
            return val.trim().isEmpty();
        } else if(obj instanceof Long) {
            Long val = (Long) obj;
            return val == 0l;
        } else if(obj instanceof Integer) {
            Integer val = (Integer) obj;
            return val == 0;
        } else if(obj instanceof Float) {
            Float val = (Float) obj;
            return val == 0.0f;
        } else if(obj instanceof Double) {
            Double val = (Double) obj;
            return val == 0.0d;
        }

        return false;
    }

    public static String createId(IDType type) {
        long time = (new Date()).getTime();
        switch(type) {
            case ADDRESS:
                return ("ADD" + time);
            case CHECKOUT:
                return ("OD" + time);
            case WISHLIST:
                return ("WL" + time);
            case CART:
                return ("CRT" + time);
            default:
                return ("" + time);
        }
    }
}
