package com.mark6.api.helper;

public enum DateFormat {
    DATE_WITH_TIME("yyyy-MM-dd HH:mm:ss"),
    DATE_TIME_DAY("E, MMM d''yy HH:mm a"),
    DATE("E, MMM d, yyyy");

    private String format;

    DateFormat(String format) {
        this.format = format;
    }

    public String getFormat() {
        return this.format;
    }
}
