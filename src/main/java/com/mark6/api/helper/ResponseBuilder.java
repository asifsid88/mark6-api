package com.mark6.api.helper;

import com.mark6.api.constants.ResponseStatus;
import com.mark6.api.model.Response;

/**
 * Created by mhussaa on 9/10/17.
 */
public class ResponseBuilder {

    public static Response buildResponse(Object data) {
        Response response;
        if(Util.isNullOrEmpty(data)) {
            response = ResponseBuilder.buildResponse(ResponseStatus.FAIL, new Object());
        } else {
            response = ResponseBuilder.buildResponse(ResponseStatus.OK, data);
        }

        return response;
    }

    private static Response buildResponse(ResponseStatus responseStatus, Object data) {
        Response response = new Response();
        response.setStatus(responseStatus.getCode());
        response.setMessage(responseStatus.getDescription());
        response.setData(data);

        return response;
    }
}
