package com.mark6.api.services;

import com.mark6.api.dal.ProductDaoService;
import com.mark6.api.model.ProductDetail;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mhussaa on 9/10/17.
 */
@Service
@Log4j2
public class ProductService {

    private ProductDaoService productDAOService;

    @Autowired
    public ProductService(ProductDaoService productDAOService) {
        this.productDAOService = productDAOService;
    }

    public ProductDetail getProductById(String productId) {
        return productDAOService.getProductById(productId);
    }

    public ProductDetail findProductByStyleCode(String styleCode) {
        return productDAOService.findProductByStyleCode(styleCode);
    }

    public ProductDetail createProduct(ProductDetail productDetail) {
        return productDAOService.save(productDetail);
    }
}
