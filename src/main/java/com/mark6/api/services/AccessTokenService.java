package com.mark6.api.services;

import com.mark6.api.dal.AccessTokenDaoService;
import com.mark6.api.helper.Util;
import com.mark6.api.model.AccessToken;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Log4j2
public class AccessTokenService {

    private AccessTokenDaoService accessTokenDaoService;

    @Autowired
    public AccessTokenService(AccessTokenDaoService accessTokenDaoService) {
        this.accessTokenDaoService = accessTokenDaoService;
    }

    public String getAccessToken(String customerId) {
        AccessToken accessToken = accessTokenDaoService.getAccessToken(customerId);
        if(Util.isNullOrEmpty(accessToken) || accessTokenDaoService.isAccessTokenExpired(accessToken)) {
            accessToken = accessTokenDaoService.createAccessToken(customerId);
        }

        return accessToken.getValue();
    }

    public AccessToken getAccessToken() {
        return accessTokenDaoService.createAccessToken(UUID.randomUUID().toString());
    }

    public boolean logout(String customerId) {
        return accessTokenDaoService.logout(customerId);
    }
}
