package com.mark6.api.services;

import com.mark6.api.dal.OrderDaoService;
import com.mark6.api.helper.ModelHelper;
import com.mark6.api.model.Checkout;
import com.mark6.api.model.Order;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mhussaa on 9/10/17.
 */
@Service
@Log4j2
public class OrderService {

    private OrderDaoService orderDaoService;
    private ModelHelper modelHelper;

    @Autowired
    public OrderService(OrderDaoService orderDaoService, ModelHelper modelHelper) {
        this.orderDaoService = orderDaoService;
        this.modelHelper = modelHelper;
    }

    public String placeOrder(Checkout checkout) {
        Order order = modelHelper.createOrderFromCheckout(checkout);
        orderDaoService.saveOrder(order);

        return order.getId();
    }

    public Order getOrderDetails(String orderId) {
        return orderDaoService.getOrderById(orderId);
    }

    public List<Order> getAllOrdersByCustomerId(String customerId) {
        return orderDaoService.getAllOrdersByCustomerId(customerId);
    }
}
