package com.mark6.api.services;

import com.mark6.api.dal.CartDaoService;
import com.mark6.api.helper.ModelHelper;
import com.mark6.api.helper.Util;
import com.mark6.api.model.Cart;
import com.mark6.api.model.CartItem;
import com.mark6.api.model.ProductDetail;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mhussaa on 9/10/17.
 */
@Service
@Log4j2
public class CartService {

    private CartDaoService cartDaoService;
    private ModelHelper modelHelper;

    @Autowired
    public CartService(CartDaoService cartDaoService, ModelHelper modelHelper) {
        this.cartDaoService = cartDaoService;
        this.modelHelper = modelHelper;
    }

    public Cart addToCart(String customerId, String productId, int quantity, ProductDetail productDetail) {
        Cart cartRef = cartDaoService.getCartRefId(customerId);

        modelHelper.addCartItem(cartRef, productDetail, productId, quantity);
        return updateCart(cartRef);
    }

    public Cart getCartRef(String customerId) {
        Cart cartRef = getNonTransientCart(customerId);
        return mergeCart(cartRef, customerId);
    }

    public Cart createTransientCartRef(String customerId, String productId, ProductDetail productDetail) {
        Cart cartRef = cartDaoService.createTransientCart(customerId);
        modelHelper.addCartItem(cartRef, productDetail, productId, 1);
        return updateCart(cartRef);
    }

    public Cart removeCartItem(String cartRefId, String customerId, String cartItemId) {
        Cart cartRef = cartDaoService.getCart(cartRefId);
        if(Util.isNullOrEmpty(cartRef) || Util.isNullOrEmpty(cartRef.getId())) {
            return null;
        } else if(!cartRef.getCustomerId().equals(customerId)) {
            return null;
        }

        cartRef.getCartItems().removeIf(cartItem -> cartItem.getM6sin().equals(cartItemId));
        return updateCart(cartRef);
    }

    public Cart updateQuantity(String cartRefId, String customerId, String cartItemId, String quantity) {
        Cart cartRef = cartDaoService.getCart(cartRefId);
        if(Util.isNullOrEmpty(cartRef) || Util.isNullOrEmpty(cartRef.getId())) {
            return null;
        } else if(!cartRef.getCustomerId().equals(customerId)) {
            return null;
        }

        for(CartItem cartItem : cartRef.getCartItems()) {
            if(cartItem.getM6sin().equals(cartItemId)) {
                cartItem.setQuantity(Integer.parseInt(quantity));
                break;
            }
        }

        return updateCart(cartRef);
    }

    public void deleteCart(String cartRefId) {
        cartDaoService.deleteCart(cartRefId);
    }

    private Cart getNonTransientCart(String customerId) {
        Cart cartRef = cartDaoService.getCartRefId(customerId);
        if(Util.isNullOrEmpty(cartRef) || Util.isNullOrEmpty(cartRef.getId())) {
            // No cart is associated to customer. Create empty cart.
            cartRef = cartDaoService.createCart(customerId);
        }

        return cartRef;
    }

    private Cart mergeCart(Cart cart, String customerId) {
        Cart transientCartRef = cartDaoService.getTransientCart(customerId);
        if(!Util.isNullOrEmpty(transientCartRef) && !Util.isNullOrEmpty(transientCartRef.getId())) {
            mergeCart(cart, transientCartRef);
            cartDaoService.deleteCart(transientCartRef.getId());
        }

        return updateCart(cart);
    }

    private void mergeCart(Cart cart, Cart transientCart) {
        if(transientCart.getCartItems().size() == 0) {
            return;
        }

        String transientProductId = transientCart.getCartItems().get(0).getM6sin();
        boolean isAlreadyPresent = Boolean.FALSE;
        for(CartItem cartItem : cart.getCartItems()) {
            if(cartItem.getM6sin().equals(transientProductId)) {
                isAlreadyPresent = Boolean.TRUE;
            }
        }

        if(!isAlreadyPresent) {
            cart.getCartItems().addAll(transientCart.getCartItems());
        }
    }

    private Cart updateCart(Cart cart) {
        cart = cartDaoService.updateCart(cart);
        cart.setAmountPayable(modelHelper.getAmountPayable(cart.getCartItems()));
        return cart;
    }
}
