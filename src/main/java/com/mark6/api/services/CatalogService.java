package com.mark6.api.services;

import com.mark6.api.constants.CatalogType;
import com.mark6.api.dal.CatalogDaoService;
import com.mark6.api.model.Catalog;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mhussaa on 9/10/17.
 */
@Service
@Log4j2
public class CatalogService {

    private CatalogDaoService catalogDaoService;

    @Autowired
    public CatalogService(CatalogDaoService catalogDaoService) {
        this.catalogDaoService = catalogDaoService;
    }

    public Catalog getCatalog(CatalogType catalogType) {
        return catalogDaoService.getCatalog(catalogType);
    }
}
