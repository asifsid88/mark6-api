package com.mark6.api.services;

import com.mark6.api.dal.CheckoutDaoService;
import com.mark6.api.helper.ModelHelper;
import com.mark6.api.model.Cart;
import com.mark6.api.model.Checkout;
import com.mark6.api.model.Customer;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mhussaa on 9/10/17.
 */
@Service
@Log4j2
public class CheckoutService {

    private CheckoutDaoService checkoutDaoService;
    private ModelHelper modelHelper;

    @Autowired
    public CheckoutService(CheckoutDaoService checkoutDaoService, ModelHelper modelHelper) {
        this.checkoutDaoService = checkoutDaoService;
        this.modelHelper = modelHelper;
    }

    public Checkout init(Cart cart, Customer customer) {
        Checkout checkout = modelHelper.createCheckoutObject(cart, customer);
        return checkoutDaoService.init(checkout);
    }

    public void deleteCheckout(String checkoutId) {
        checkoutDaoService.deleteCheckout(checkoutId);
    }
}
