package com.mark6.api.services;

import com.mark6.api.dal.CustomerDaoService;
import com.mark6.api.dal.WishlistDaoService;
import com.mark6.api.helper.DateFormat;
import com.mark6.api.helper.IDType;
import com.mark6.api.helper.ModelHelper;
import com.mark6.api.helper.Util;
import com.mark6.api.model.Address;
import com.mark6.api.model.Customer;
import com.mark6.api.model.CustomerDetail;
import com.mark6.api.model.Wishlist;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Log4j2
public class CustomerService {

    private CustomerDaoService customerDaoService;
    private WishlistDaoService wishlistDaoService;
    private ModelHelper modelHelper;

    @Autowired
    public CustomerService(CustomerDaoService customerDaoService, WishlistDaoService wishlistDaoService,
                           ModelHelper modelHelper) {
        this.customerDaoService = customerDaoService;
        this.wishlistDaoService = wishlistDaoService;
        this.modelHelper = modelHelper;
    }

    public Customer getCustomerById(String customerId) {
        return customerDaoService.getCustomerById(customerId);
    }

    public CustomerDetail getCustomerDetail(String customerId) {
        CustomerDetail customerDetail = customerDaoService.getCustomerDetail(customerId);

        Wishlist wishlist = wishlistDaoService.getWishlist(customerId);
        List<String> wishlistProductIdList = wishlist.getCartItems()
                                                    .stream()
                                                    .map(cartItem -> cartItem.getProductId())
                                                    .collect(Collectors.toList());
        customerDetail.setWishlist(wishlistProductIdList);

        return customerDetail;
    }

    public Customer getCustomerByEmail(String email) {
        return customerDaoService.getCustomerByEmail(email);
    }

    public Customer createCustomer(Customer customer) {
        return customerDaoService.createCustomer(customer);
    }

    public List<Address> getAddressList(String customerId) {
        return customerDaoService.getAddressList(customerId);
    }

    public Address getCustomerAddressById(String customerId, String addressId) {
        List<Address> addressList = getAddressList(customerId);
        return getAddressById(addressList, addressId);
    }

    public Address saveAddress(String customerId, Address address) {
        CustomerDetail customerDetail = getCustomerDetail(customerId);
        if(address.getId() == null) {
            address.setId(Util.createId(IDType.ADDRESS));
        } else {
            customerDetail.getAddressList().removeIf(currentAddress -> currentAddress.getId().equals(address.getId()));
        }
        address.setCreationDate(Util.getCurrentDate(DateFormat.DATE_WITH_TIME));
        address.setUpdateDate(Util.getCurrentDate(DateFormat.DATE_WITH_TIME));
        customerDetail.getAddressList().add(address);

        CustomerDetail updatedCustomerDetail = customerDaoService.saveCustomerDetail(customerDetail);
        return getAddressById(updatedCustomerDetail.getAddressList(), address.getId());
    }

    public CustomerDetail updateCustomerDetail(String customerId, Customer customer) {
        Customer updatedCustomer = getCustomerById(customerId);
        updatedCustomer = modelHelper.mergeCustomer(updatedCustomer, customer);
        updatedCustomer = customerDaoService.saveCustomer(updatedCustomer);

        CustomerDetail updatedCustomerDetail = getCustomerDetail(customerId);
        updatedCustomerDetail = modelHelper.updateCustomerDetail(updatedCustomerDetail, updatedCustomer);
        return customerDaoService.saveCustomerDetail(updatedCustomerDetail);
    }

    public void deleteAddress(String customerId, String addressId) {
        CustomerDetail customerDetail = getCustomerDetail(customerId);
        customerDetail.getAddressList().removeIf(currentAddress -> currentAddress.getId().equals(addressId));
        customerDaoService.saveCustomerDetail(customerDetail);
    }

    public List<Address> setDefaultAddress(String customerId, String addressId) {
        CustomerDetail customerDetail = getCustomerDetail(customerId);
        List<Address> updatedAddress = new LinkedList<>();
        for(Address address : customerDetail.getAddressList()) {
            if(address.isDefault() && address.getId().equals(addressId)) {
                updatedAddress.add(address);
            } else {
                if(address.isDefault()) {
                    address.setDefault(Boolean.FALSE);
                    updatedAddress.add(address);
                } else if(address.getId().equals(addressId)) {
                    address.setDefault(Boolean.TRUE);
                    updatedAddress.add(address);
                }
            }
        }
        customerDaoService.saveCustomerDetail(customerDetail);
        return updatedAddress;
    }

    private Address getAddressById(List<Address> addressList, String addressId) {
        for(Address address : addressList) {
            if(address.getId().equals(addressId)) {
                return address;
            }
        }

        return null;
    }
}
