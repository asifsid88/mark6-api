package com.mark6.api.services;

import com.mark6.api.dal.WishlistDaoService;
import com.mark6.api.helper.ModelHelper;
import com.mark6.api.helper.Util;
import com.mark6.api.model.ProductDetail;
import com.mark6.api.model.Wishlist;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mhussaa on 9/10/17.
 */
@Service
@Log4j2
public class WishlistService {

    private WishlistDaoService wishlistDaoService;
    private ModelHelper modelHelper;

    @Autowired
    public WishlistService(WishlistDaoService wishlistDaoService, ModelHelper modelHelper) {
        this.wishlistDaoService = wishlistDaoService;
        this.modelHelper = modelHelper;
    }

    public Wishlist addToWishlist(String customerId, String productId, ProductDetail productDetail) {
        Wishlist wishlist = wishlistDaoService.getWishlist(customerId);
        modelHelper.addWishlistItem(wishlist, productDetail, productId);
        wishlist = wishlistDaoService.updateWishlist(wishlist);

        return wishlist;
    }

    public Wishlist getWishlist(String customerId) {
        return wishlistDaoService.getWishlist(customerId);
    }

    public Wishlist removeWishlistItem(String customerId, String productId) {
        Wishlist wishlist = wishlistDaoService.getWishlist(customerId);
        if(Util.isNullOrEmpty(wishlist) || Util.isNullOrEmpty(wishlist.getId())) {
            return null;
        }

        wishlist.getCartItems().removeIf(cartItem -> cartItem.getProductId().equals(productId));
        wishlist = wishlistDaoService.updateWishlist(wishlist);

        return wishlist;
    }
}
